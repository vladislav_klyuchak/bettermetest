package com.vkluchak.bettermemovies.ui.favorites

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vkluchak.bettermemovies.R
import com.vkluchak.bettermemovies.di.Injectable
import com.vkluchak.bettermemovies.ui.adapter.MoviesPagedAdapter
import com.vkluchak.bettermemovies.util.networkState
import com.vkluchak.bettermemovies.util.shareMovie
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.android.synthetic.main.view_global_error.*
import javax.inject.Inject

class FavoritesFragment : Fragment(), Injectable {

    private var moviesPagedAdapter: MoviesPagedAdapter? = null
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val model: FavoritesViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)
            .get(FavoritesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_notifications, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moviesPagedAdapter = MoviesPagedAdapter()
        val lLManagerVertical =
            GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)

        rvBookmarkMovies?.layoutManager = lLManagerVertical
        rvBookmarkMovies?.adapter = moviesPagedAdapter

        moviesPagedAdapter?.onFavoriteClickListener = {
            model.setIsBookmark(it)
        }
        moviesPagedAdapter?.shareClickListener = {
            shareMovie(requireContext(), it)
        }

        model.getAllBookmarkMovies().observe(
            viewLifecycleOwner,
            Observer {
                if (it.isNullOrEmpty()) {
                    clEmptyContainer.visibility = VISIBLE
                } else {
                    clEmptyContainer.visibility = GONE
                }

                moviesPagedAdapter?.setItems(it)
            })

        mbtnRetry.setOnClickListener {
            model.syncData()
        }
        model.mSavedWorkInfo.observe(
            viewLifecycleOwner,
            Observer {
                if (!it.isNullOrEmpty()) {
                    requireActivity().networkState(it[0].state) {
                        model.syncData()
                    }
                    Log.i("worker mSavedWorkInfo", it.toString())
                }
                initSwipeToRefresh()
            })
    }

    private fun initSwipeToRefresh() {
        swipeBookmarkRefreshContainer?.setColorSchemeResources(
            R.color.colorAccent,
            R.color.colorAccent,
            R.color.colorAccent
        )

        swipeBookmarkRefreshContainer?.setOnRefreshListener {
            model.syncData()
            swipeBookmarkRefreshContainer?.isRefreshing = false
        }
    }
}
