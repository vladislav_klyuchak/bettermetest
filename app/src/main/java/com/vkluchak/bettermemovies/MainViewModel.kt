package com.vkluchak.bettermemovies

import androidx.lifecycle.ViewModel
import androidx.work.*
import com.vkluchak.bettermemovies.worker.SYNC_DATA_WORK_NAME
import com.vkluchak.bettermemovies.worker.SyncWorker
import com.vkluchak.bettermemovies.worker.TAG_SYNC_DATA
import com.vkluchak.bettermemovies.worker.createSyncPeriodicWork
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(): ViewModel(){

    fun fetchData() {
        createSyncPeriodicWork()
    }
}