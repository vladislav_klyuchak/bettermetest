package com.vkluchak.data.repository.network

import com.vkluchak.data.entity.BaseDataListEntity
import com.vkluchak.data.entity.MovieEntity
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesService {

    @GET("discover/movie")
    fun getMoviesListAsync(
        @Query("page") pageCount: Int,
        @Query("primary_release_date.gte") fromDate: String,
        @Query("primary_release_date.lte") toDate: String,
        @Query("api_key") apiKey: String = "782cb2026dbf21f60c4baa9537275da8" // TODO move it
    ): Deferred<Response<BaseDataListEntity<MovieEntity>>>

}