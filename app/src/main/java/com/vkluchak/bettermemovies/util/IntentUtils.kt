package com.vkluchak.bettermemovies.util

import android.content.Context
import android.content.Intent
import com.vkluchak.bettermemovies.R
import com.vkluchak.domain.model.MovieData


fun shareMovie(context: Context, movie: MovieData) {
    val shareBody = "https://www.themoviedb.org/movie/${movie.id}"
    val sharingIntent = Intent(Intent.ACTION_SEND)
    sharingIntent.type = "text/plain"
   // sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here")
    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
    context.startActivity(
        Intent.createChooser(
            sharingIntent,
            context.resources.getString(R.string.share_movie, movie.title)
        )
    )
}