package com.vkluchak.bettermemovies.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable