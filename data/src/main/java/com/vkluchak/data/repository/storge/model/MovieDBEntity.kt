package com.vkluchak.data.repository.storge.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.vkluchak.data.repository.storge.converter.GenreIdsConverter

/**
 * Immutable model class for a Movie and entity in the Room database.
 */
@Entity(tableName = "movies")
data class MovieDBEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "vote_count")
    var voteCount: Int,
    @ColumnInfo(name = "video")
    var video: Boolean,
    @ColumnInfo(name = "vote_average")
    var voteAverage: Float,
    @ColumnInfo(name = "title")
    var title: String,
    @ColumnInfo(name = "popularity")
    var popularity: Float,
    @ColumnInfo(name = "poster_path")
    var posterPath: String? = null,
    @ColumnInfo(name = "original_language")
    var originalLanguage: String,
    @ColumnInfo(name = "original_title")
    var originalTitle: String,
    @ColumnInfo(name = "backdrop_path")
    var backdropPath: String? = null,
    @ColumnInfo(name = "adult")
    var adult: Boolean,
    @ColumnInfo(name = "overview")
    var overview: String,
    @ColumnInfo(name = "release_date")
    var releaseDate: String,
    @ColumnInfo(name = "genre_ids")
    @TypeConverters(GenreIdsConverter::class)
    var genreIds: List<Int>?,
    @ColumnInfo(name = "is_bookmark")
    var isBookmark: Boolean = false
)