package com.vkluchak.data.repository.network

import android.net.ConnectivityManager

interface ConnectionManager {
    fun isNetworkAbsent() : Boolean
}

class ConnectionManagerImpl(private val connectivityManager: ConnectivityManager) :
    ConnectionManager {

    override fun isNetworkAbsent(): Boolean {
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo == null || !netInfo.isConnected
    }
}