package com.vkluchak.bettermemovies

import com.vkluchak.domain.model.MovieData

val MOVIE_TEST_LIST = listOf(
    MovieData(
        adult = false,
        backdrop_path = "/6nuYKOPeNPPfJoHeP1wbAatNdJV.jpg",
        genre_ids = listOf(80, 18),
        id = 339395,
        original_language = "en",
        original_title = "Georgetown",
        overview = "Ulrich Mott, an ambitious social climber, marries a wealthy widow in Washington D.C. in order to mix with powerful political players.",
        popularity = 9.491F,
        poster_path = "/tA1anonu6ZDcrtUxstdGxrLw2dM.jpg",
        release_date = "2020-04-23",
        title = "Georgetown",
        video = false,
        vote_average = 6.0F,
        vote_count = 1,
        is_bookmark = false
    ),
    MovieData(
        adult = false,
        backdrop_path = null,
        genre_ids = null,
        id = 445574,
        original_language = "fr",
        original_title = "Le Misanthrope",
        overview = "Alceste loves Célimène, a flirtatious woman from the Parisian high society. He loathes this world for its hypocritical etiquette but, shaken by a public trial he is called to by this social circle, he must visit Célimène to ask for her help…",
        popularity = 3.515f,
        poster_path = "/dG2jn14HSqSnbrGIn0jtiIzV7pg.jpg",
        release_date = "2020-04-19",
        title = "Le Misanthrope",
        video = false,
        vote_average = 7.0f,
        vote_count = 1,
        is_bookmark = true
    ),
    MovieData(
        adult = false,
        backdrop_path = "/pXGrzSuh4SsJz98V57b4HRE2NZz.jpg",
        genre_ids = listOf(80, 53),
        id = 461479,
        original_language = "ta",
        original_title = "நரகாசூரன்",
        overview = "Naragasooran is an upcoming tamil thriller movie directed by Karthick Naren, starring Sundeep Kishan, Aravidswamy, Shriya Saran & Indrajith",
        popularity = 26.219f,
        poster_path = "/r7ne4UKFRfemhIdZOMCDw973b14.jpg",
        release_date = "2020-04-29",
        title = "Naragasooran",
        video = false,
        vote_average = 0.0f,
        vote_count = 0,
        is_bookmark = true
    ),
    MovieData(
        adult = false,
        backdrop_path = "/gxKnr0hVvJId7TqS0rjdX79zwQU.jpg",
        genre_ids = listOf(10749, 18),
        id = 488707,
        original_language = "ko",
        original_title = "그녀의 비밀정원",
        overview = "Two brothers fell in love with one woman, Hyun-jae. But she chose Chung-seo. And Jang-seo has been living bitter days ever since the betrayal of his younger brother and his lover. After 17 years passed, Jang-seo gets a call from her. He does not know why she’s coming back, but his heart is beating again.",
        popularity = 5.174f,
        poster_path = "/8aeItpTWUUQmVyqkvFwYV1XwGLO.jpg",
        release_date = "2020-04-22",
        title = "Invitation",
        video = false,
        vote_average = 0.0f,
        vote_count = 0,
        is_bookmark = false
    )
)