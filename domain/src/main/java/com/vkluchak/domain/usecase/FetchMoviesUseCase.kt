package com.vkluchak.domain.usecase

import com.vkluchak.domain.model.MovieData
import com.vkluchak.domain.repository.MoviesRepository

interface FetchMoviesUseCase {
    suspend fun syncData(fromDate: String, toDate: String)
}

class FetchMoviesUseCaseImpl(private val moviesRepository: MoviesRepository) : FetchMoviesUseCase {

    override suspend fun syncData(fromDate: String, toDate: String) {
        val movieList = getMoviesAllPages(1, mutableListOf(), fromDate, toDate)
        moviesRepository.insertMovieList(movieList)
    }

    private suspend fun getMoviesAllPages(
        pageCount: Int,
        resultList: MutableList<MovieData>,
        fromDate: String,
        toDate: String
    ): List<MovieData>? {
        val list = moviesRepository.getMoviesListAsync(
            pageCount, fromDate, toDate
        )
        return if (list.isNullOrEmpty()) {
            resultList
        } else {
            resultList.addAll(list)
            getMoviesAllPages(pageCount + 1, resultList, fromDate, toDate)
        }
    }
}