package com.vkluchak.bettermemovies.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.vkluchak.bettermemovies.worker.TAG_SYNC_DATA
import com.vkluchak.bettermemovies.worker.createSyncWork
import com.vkluchak.data.repository.storge.model.NetworkState
import com.vkluchak.domain.model.MovieData
import com.vkluchak.domain.repository.MoviesRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class DashboardViewModel @Inject constructor(
    private val repository: MoviesRepository
) : ViewModel() {

    var mSavedWorkInfo: LiveData<List<WorkInfo>> =
        WorkManager.getInstance().getWorkInfosByTagLiveData(TAG_SYNC_DATA)

    val networkState = MutableLiveData<NetworkState>()

    fun getAllMovies() = repository.getMoviesLiveData()

    fun changeBookmarkStatus(movieData: MovieData) {
        viewModelScope.launch(Dispatchers.IO + getJobErrorHandler()) {
            //  networkState.postValue(NetworkState.Running)
            repository.changeBookmarkStatus(movieData.id, !movieData.is_bookmark)
            //  networkState.postValue(NetworkState.Success)
        }
    }

    fun syncData() {
        createSyncWork()
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        Log.e(DashboardViewModel::class.java.simpleName, "An error happened: ${e.toString()}")
        networkState.postValue(NetworkState.Failed(e.localizedMessage))
    }
}