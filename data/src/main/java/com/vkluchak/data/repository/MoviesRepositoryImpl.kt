package com.vkluchak.data.repository

import androidx.lifecycle.LiveData
import com.vkluchak.data.entity.mapper.BasePaginationMapper
import com.vkluchak.data.entity.mapper.MovieDataBaseMapper
import com.vkluchak.data.entity.mapper.MovieDataMapper
import com.vkluchak.data.repository.network.ConnectionManager
import com.vkluchak.data.repository.network.MoviesService
import com.vkluchak.data.repository.storge.MovieDao
import com.vkluchak.domain.model.MovieData
import com.vkluchak.domain.repository.MoviesRepository

class MoviesRepositoryImpl(
    private val connectionManager: ConnectionManager,
    private val moviesService: MoviesService,
    private val movieDao: MovieDao
) : MoviesRepository {

    override suspend fun getMoviesListAsync(
        pageCount: Int,
        fromDate: String,
        toDate: String
    ) =
        if (connectionManager.isNetworkAbsent()) {
            throw Throwable()
        } else {
            val launchesAsync =
                moviesService.getMoviesListAsync(pageCount, fromDate, toDate)

            val result = launchesAsync.await()
            if (result.isSuccessful) {
                result.body()
                    ?.let { BasePaginationMapper(dataMapper = MovieDataMapper()).map(it) }
            } else {
                result.errorBody().let {
                    throw Throwable(it.toString())
                }
            }
        }

    override fun getMoviesLiveData(): LiveData<List<MovieData>> = movieDao.getAllMovies()

    override fun getBookmarkMoviesLiveData(): LiveData<List<MovieData>> =
        movieDao.getAllBookmarkMovies()

    override suspend fun changeBookmarkStatus(movieId: Int, is_bookmark: Boolean) {
        movieDao.insertBookmark(movieId, is_bookmark)
    }

    override suspend fun insertMovieList(movieList: List<MovieData>?) {
        movieDao.insertMovie(movieList?.map { MovieDataBaseMapper().map(it) })
    }

}
