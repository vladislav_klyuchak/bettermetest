package com.vkluchak.data.repository.storge.model

sealed class NetworkState {
    object Running : NetworkState()
    object Success : NetworkState()
    data class Failed(val errorMassage : String?) : NetworkState()
}