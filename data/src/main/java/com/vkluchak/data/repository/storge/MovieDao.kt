package com.vkluchak.data.repository.storge

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vkluchak.data.repository.storge.model.MovieDBEntity
import com.vkluchak.domain.model.MovieData

@Dao
interface MovieDao {
    /**
     * Get the Movies from the table.
     * -------------------------------
     * Since the DB use as caching, we don't return LiveData.
     * We don't need to get update every time the database update.
     * We using the get query when application start. So, we able to display
     * data fast and in case we don't have connection to work offline.
     * @return the movies from the table
     */
    @Query("SELECT * FROM MOVIES ORDER BY release_date DESC")
    fun getAllMovies(): LiveData<List<MovieData>>

    /**
     * Insert a movie in the database. If the movie already exists, ignore it.
     *
     * @param movieDBEntity the movie to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovie(movieDBEntity: List<MovieDBEntity>?)

    @Query("SELECT * FROM MOVIES WHERE is_bookmark = 1")
    fun getAllBookmarkMovies(): LiveData<List<MovieData>>

    @Query("UPDATE movies SET is_bookmark= :isBookmark WHERE id = :movieId")
    fun insertBookmark(movieId: Int, isBookmark: Boolean)

    @Query("DELETE FROM movies")
    fun deleteAllMovies()
}