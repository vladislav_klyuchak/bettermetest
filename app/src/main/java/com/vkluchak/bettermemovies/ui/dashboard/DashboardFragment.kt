package com.vkluchak.bettermemovies.ui.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vkluchak.bettermemovies.R
import com.vkluchak.bettermemovies.di.Injectable
import com.vkluchak.bettermemovies.ui.adapter.MoviesPagedAdapter
import com.vkluchak.bettermemovies.util.networkState
import com.vkluchak.bettermemovies.util.shareMovie
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.view_global_error.*
import javax.inject.Inject


class DashboardFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var moviesPagedAdapter: MoviesPagedAdapter? = null

    private val model: DashboardViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)
            .get(DashboardViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moviesPagedAdapter = MoviesPagedAdapter()
        val lLManagerVertical =
            GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)

        moviesPagedAdapter?.onFavoriteClickListener = {
            model.changeBookmarkStatus(it)
        }
        moviesPagedAdapter?.shareClickListener = {
            shareMovie(requireContext(), it)
        }


        rvMovies?.layoutManager = lLManagerVertical
        rvMovies?.adapter = moviesPagedAdapter

        model.getAllMovies().observe(
            viewLifecycleOwner,
            Observer {
                if (it.isNullOrEmpty()) {
                    clEmptyContainer.visibility = View.VISIBLE
                } else {
                    clEmptyContainer.visibility = View.GONE
                }
                moviesPagedAdapter?.setItems(it)
            })

        model.networkState.observe(
            viewLifecycleOwner,
            Observer {
                requireActivity().networkState(it) {
                    model.syncData()
                }
            })

        mbtnRetry.setOnClickListener {
            model.syncData()
        }

        initSwipeToRefresh()

        model.mSavedWorkInfo.observe(
            viewLifecycleOwner,
            Observer {
                if (!it.isNullOrEmpty()) {
                    requireActivity().networkState(it[0].state) {
                        model.syncData()
                    }
                    Log.i("worker mSavedWorkInfo", it.toString())
                }
            }
        )
    }

    private fun initSwipeToRefresh() {
        swipeRefreshContainer?.setColorSchemeResources(
            R.color.colorAccent,
            R.color.colorAccent,
            R.color.colorAccent
        )

        swipeRefreshContainer?.setOnRefreshListener {
            model.syncData()
            swipeRefreshContainer?.isRefreshing = false
        }
    }
}
