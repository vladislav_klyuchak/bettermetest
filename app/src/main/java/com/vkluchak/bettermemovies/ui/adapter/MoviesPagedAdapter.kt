package com.vkluchak.bettermemovies.ui.adapter

import android.animation.ObjectAnimator
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vkluchak.bettermemovies.R
import com.vkluchak.bettermemovies.util.inflate
import com.vkluchak.bettermemovies.util.loadFromUrl
import com.vkluchak.domain.model.MovieData
import kotlinx.android.synthetic.main.item_movie_card.view.*

class MoviesPagedAdapter(
) : RecyclerView.Adapter<MoviesPagedAdapter.SimpleHolder>() {


    private var items = mutableListOf<MovieData>()
    internal var shareClickListener: (MovieData) -> Unit = {}
    internal var onFavoriteClickListener: (MovieData) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SimpleHolder(parent.inflate(R.layout.item_movie_card))

    override fun onBindViewHolder(holder: MoviesPagedAdapter.SimpleHolder, position: Int) {
        holder.bind(items[position], shareClickListener, onFavoriteClickListener)
    }

    override fun onBindViewHolder(holder: SimpleHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val combinedChange = createCombinedPayload(payloads as List<Change<MovieData>>)
            val oldData = combinedChange.oldData
            val newData = combinedChange.newData

            if (newData.is_bookmark != oldData.is_bookmark) {
                if (newData.is_bookmark) {
                    ObjectAnimator.ofFloat(holder.itemView.ibFavorite, "rotation", 0f, 360f).apply {
                        duration = 500
                        start()
                    }
                    holder.itemView.ibFavorite.setImageResource(R.drawable.ic_favorite_enable)
                } else {
                    holder.itemView.ibFavorite.setImageResource(R.drawable.ic_favorite)
                }
                holder.itemView.ibFavorite.setOnClickListener {
                    onFavoriteClickListener(newData)
                }
            }
        }
    }

    class SimpleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            item: MovieData,
            clickListener: (MovieData) -> Unit,
            onFavoriteClickListener: (MovieData) -> Unit
        ) {
            itemView.ivImage.loadFromUrl(item.poster_path)

            itemView.tvName.text = item.title
            itemView.rbRating.rating = item.vote_average
            itemView.tvRatingCount.text = "(${item.vote_count})"
            itemView.ibFavorite.setOnClickListener {
                onFavoriteClickListener(item)
            }


            if (item.is_bookmark) {
                itemView.ibFavorite.setImageResource(R.drawable.ic_favorite_enable)
            } else {
                itemView.ibFavorite.setImageResource(R.drawable.ic_favorite)
            }
            itemView.tvOverview.text = item.overview
            itemView.cardContainer.setOnClickListener { clickListener(item) }
        }
    }

    fun setItems(newItems: List<MovieData>) {
        val result = DiffUtil.calculateDiff(MovieDataDiffUtilCallback(this.items, newItems))
        result.dispatchUpdatesTo(this)
        this.items.clear()
        this.items.addAll(newItems)
    }

    class MovieDataDiffUtilCallback(
        private var oldItems: List<MovieData>,
        private var newItems: List<MovieData>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition].id == newItems[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition] == newItems[newItemPosition]

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return Change(
                oldItem,
                newItem)
        }
    }

    override fun getItemCount(): Int = items.size
}

data class Change<out T>(
    val oldData: T,
    val newData: T
)

fun <T> createCombinedPayload(payloads: List<Change<T>>): Change<T> {
    assert(payloads.isNotEmpty())
    val firstChange = payloads.first()
    val lastChange = payloads.last()

    return Change(firstChange.oldData, lastChange.newData)
}
