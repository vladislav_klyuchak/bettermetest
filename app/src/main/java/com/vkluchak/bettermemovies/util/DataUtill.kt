package com.vkluchak.bettermemovies.util

import java.text.SimpleDateFormat
import java.util.*


const val UTC_TIMEZONE_FULL_UI_DATE_FORMAT = "yyyy-MM-dd"

fun getCurrentDate(): String =
    SimpleDateFormat(UTC_TIMEZONE_FULL_UI_DATE_FORMAT).format(Calendar.getInstance().time)

fun getDateTwoWeeksBefore(): String {
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.DAY_OF_YEAR, -14)
    return SimpleDateFormat(UTC_TIMEZONE_FULL_UI_DATE_FORMAT).format(calendar.time)
}