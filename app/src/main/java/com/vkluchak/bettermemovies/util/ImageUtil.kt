package com.vkluchak.bettermemovies.util

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.vkluchak.bettermemovies.R

private const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/"
const val SMALL_IMAGE_URL_PREFIX = IMAGE_BASE_URL + "w300"
const val BIG_IMAGE_URL_PREFIX = IMAGE_BASE_URL + "w500"

fun ImageView.loadFromUrl(url: String?) =
    Glide.with(this.context.applicationContext)
        .load("$SMALL_IMAGE_URL_PREFIX$url")
        .transition(DrawableTransitionOptions.withCrossFade())
        .placeholder(ColorDrawable(Color.LTGRAY))
        .error(R.drawable.ic_image_placeholder)
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .into(this)