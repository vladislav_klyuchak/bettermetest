package com.vkluchak.bettermemovies.worker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.*
import com.vkluchak.bettermemovies.MainActivity
import com.vkluchak.bettermemovies.R
import java.util.concurrent.TimeUnit

//TODO create Factory
fun createSyncWork() {
    val constraints = Constraints.Builder()
        .setRequiredNetworkType(NetworkType.CONNECTED)
        .build()

    val workerRequest = OneTimeWorkRequest.Builder(
        SyncWorker::class.java)
        .addTag(TAG_SYNC_DATA)
        .setInputData(workDataOf(IS_WORK_NOTIFICATION_ENABLE to false))
        .setConstraints(constraints)
        .build()

    WorkManager.getInstance().enqueueUniqueWork(
        SYNC_DATA_WORK_NAME,
        ExistingWorkPolicy.REPLACE,
        workerRequest
    )
}

fun createSyncPeriodicWork() {
    val constraints = Constraints.Builder()
        .setRequiredNetworkType(NetworkType.CONNECTED)
        .build()

    val periodicSyncDataWork = PeriodicWorkRequest.Builder(
        SyncWorker::class.java,
        12,
        TimeUnit.HOURS
    )
        .addTag(TAG_SYNC_DATA)
        .setInputData(workDataOf(IS_WORK_NOTIFICATION_ENABLE to true))
        .setConstraints(constraints)
        .build()
    WorkManager.getInstance().enqueueUniquePeriodicWork(
        SYNC_DATA_WORK_NAME,
        ExistingPeriodicWorkPolicy.REPLACE,  //Existing Periodic Work policy
        periodicSyncDataWork //work request
    )

}

fun makeStatusNotification(
    message: String?,
    context: Context
) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(
            "VERBOSE_NOTIFICATION",
            "Verbose WorkManager Notifications",
            importance
        )
        channel.description = "Shows notifications whenever work starts"
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
    val intent = Intent(context, MainActivity::class.java)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
    // Create the notification
    val builder = NotificationCompat.Builder(context, "VERBOSE_NOTIFICATION")
        .setSmallIcon(R.drawable.ic_launcher_foreground)
        .setContentTitle("Sync movies data")
        .setContentText(message)
        .setPriority(NotificationCompat.PRIORITY_HIGH) // Set the intent that will fire when the user taps the notification
        .setContentIntent(pendingIntent)
        .setVibrate(LongArray(0))
        .setAutoCancel(true)
    // Show the notification
    NotificationManagerCompat.from(context).notify(1, builder.build())
}