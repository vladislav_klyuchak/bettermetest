package com.vkluchak.domain.repository

import androidx.lifecycle.LiveData
import com.vkluchak.domain.model.MovieData

interface MoviesRepository {

    suspend fun getMoviesListAsync(pageCount: Int, fromDate: String, toDate: String): List<MovieData>?
    suspend fun insertMovieList(movieList: List<MovieData>?)
    suspend fun changeBookmarkStatus(movieId: Int, is_bookmark: Boolean)

    fun getMoviesLiveData() : LiveData<List<MovieData>>
    fun getBookmarkMoviesLiveData(): LiveData<List<MovieData>>
}