package com.vkluchak.bettermemovies.di.module

import com.vkluchak.data.repository.network.ConnectionManager
import com.vkluchak.data.repository.network.MoviesService
import com.vkluchak.data.repository.MoviesRepositoryImpl
import com.vkluchak.data.repository.storge.MovieDao
import com.vkluchak.domain.repository.MoviesRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun provideMoviesRepository(
        connectionManager: ConnectionManager,
        retrofit: Retrofit,
        movieDao: MovieDao
    ): MoviesRepository {
        return MoviesRepositoryImpl(
            connectionManager,
            retrofit.create(MoviesService::class.java),
            movieDao
        )
    }
}