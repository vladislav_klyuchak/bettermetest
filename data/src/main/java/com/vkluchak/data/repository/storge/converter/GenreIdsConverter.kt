package com.vkluchak.data.repository.storge.converter

import androidx.room.TypeConverter
import java.util.stream.Collectors

class GenreIdsConverter {
    @TypeConverter
    fun fromGenresIds(genreIds: List<Int>): String {
        return genreIds.stream().map { it.toString() }.collect(Collectors.joining(","))
    }

    @TypeConverter
    fun toGenresIds(data: String): List<Int>? {
        return if(data.isNotBlank()) {
            data.split(",").toList().map { it.toInt() }
        }else{
            null
        }
    }
}