package com.vkluchak.bettermemovies.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.vkluchak.bettermemovies.LiveDataAndroidTestUtil
import com.vkluchak.data.repository.storge.MovieDao
import com.vkluchak.data.repository.storge.MoviesDatabase
import com.vkluchak.data.repository.storge.model.MovieDBEntity
import com.vkluchak.domain.model.MovieData
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MovieDaoTest {
    private lateinit var db: MoviesDatabase

    val MOVIE_TEST: MovieDBEntity = MovieDBEntity(
        adult = false,
        backdropPath = "/gxKnr0hVvJId7TqS0rjdX79zwQU.jpg",
        genreIds = listOf(10749, 18),
        id = 488707,
        originalLanguage = "ko",
        originalTitle = "그녀의 비밀정원",
        overview = "Two brothers fell in love with one woman, Hyun-jae. But she chose Chung-seo. And Jang-seo has been living bitter days ever since the betrayal of his younger brother and his lover. After 17 years passed, Jang-seo gets a call from her. He does not know why she’s coming back, but his heart is beating again.",
        popularity = 5.174f,
        posterPath = "/8aeItpTWUUQmVyqkvFwYV1XwGLO.jpg",
        releaseDate = "2020-04-22",
        title = "Invitation",
        video = false,
        voteAverage = 0.0f,
        voteCount = 0
    )

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    fun getMovieDao(): MovieDao {
        return db.movieDao()
    }

    @Before
    @Throws(Exception::class)
    fun createDb() {
        db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            MoviesDatabase::class.java
        ).build()
    }

    @After
    @Throws(Exception::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun whenInsertMovieThenReadTheSameOne() {
        getMovieDao().deleteAllMovies()

        getMovieDao().insertMovie(listOf(MOVIE_TEST))

        val insertedMovies: List<MovieData> =
            LiveDataAndroidTestUtil<List<MovieData>>().getValue(getMovieDao().getAllMovies())

        assertNotNull(insertedMovies)
        assertEquals(insertedMovies.size, 1)
        assertEquals(insertedMovies.first().id, MOVIE_TEST.id)
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun whenInsertBookmarkReadTheSameOne() {
        getMovieDao().deleteAllMovies()

        getMovieDao().insertMovie(listOf(MOVIE_TEST))
        getMovieDao().insertBookmark(movieId = MOVIE_TEST.id, isBookmark = true)

        val bookmarkMovies: List<MovieData> =
            LiveDataAndroidTestUtil<List<MovieData>>().getValue(getMovieDao().getAllBookmarkMovies())

        assertNotNull(bookmarkMovies)
        assertEquals(bookmarkMovies.size, 1)
        assertEquals(bookmarkMovies.first().id, MOVIE_TEST.id)
    }
}