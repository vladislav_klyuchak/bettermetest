package com.vkluchak.data.entity.mapper

import com.vkluchak.data.entity.BaseDataListEntity
import com.vkluchak.domain.model.Mapper

class BasePaginationMapper<K, U>(
    private val dataMapper: Mapper<K, U>
) : Mapper<BaseDataListEntity<K>, List<U>> {
    override fun map(source: BaseDataListEntity<K>): List<U> =
        source.results.map { dataMapper.map(it) }
}