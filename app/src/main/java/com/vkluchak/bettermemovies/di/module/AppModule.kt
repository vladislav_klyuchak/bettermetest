package com.vkluchak.bettermemovies.di.module

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.room.Room
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.vkluchak.bettermemovies.BuildConfig
import com.vkluchak.data.repository.network.ConnectionManager
import com.vkluchak.data.repository.network.ConnectionManagerImpl
import com.vkluchak.data.repository.storge.MovieDao
import com.vkluchak.data.repository.storge.MoviesDatabase
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


private const val PREFS = "com.vkluchak.bettermemovies.pref"
private const val BASE_URL = "https://api.themoviedb.org/3/"
const val DATA_BASE_NAME = "BetterMovieDataBase.db"

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    @Singleton
    internal fun requestBaseUrl() = BASE_URL

    @Provides
    internal fun configureRetrofit(serverUrl: String, okHttpClient: OkHttpClient): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(serverUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())

        return builder.build()
    }

    @Singleton
    @Provides
    public fun getConnectionManager(context: Context): ConnectionManager =
        ConnectionManagerImpl(context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)

    @Singleton
    @Provides
    internal fun getBaseHttpClientBuilder(): OkHttpClient {
        val httpClient =
            OkHttpClient.Builder()
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()

            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY//HEADERS//BODY
            loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS

            httpClient.addNetworkInterceptor(loggingInterceptor)
            httpClient.addNetworkInterceptor(StethoInterceptor())
        }

        return httpClient.build()
    }

    @Provides
    @Singleton
    internal fun providePreferences(app: Application) =
        app.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideDatabase(context: Context): MoviesDatabase {
        return Room.databaseBuilder(
            context,
            MoviesDatabase::class.java,
            DATA_BASE_NAME
        ).build()
    }

    @Singleton
    @Provides
    fun provideMovieDao(db: MoviesDatabase): MovieDao {
        return db.movieDao()
    }
}