package com.vkluchak.bettermemovies.util

import android.app.Activity
import android.view.View
import com.vkluchak.bettermemovies.MainActivity
import kotlinx.android.synthetic.main.activity_main.*

fun Activity.progress(isVisible: Boolean) {
    progressBar?.visibility = if (isVisible) View.VISIBLE else View.GONE
}