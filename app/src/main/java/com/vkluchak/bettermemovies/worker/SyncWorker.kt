package com.vkluchak.bettermemovies.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.vkluchak.bettermemovies.R
import com.vkluchak.bettermemovies.util.getCurrentDate
import com.vkluchak.bettermemovies.util.getDateTwoWeeksBefore
import com.vkluchak.domain.usecase.FetchMoviesUseCase

const val TAG_SYNC_DATA = "TAG_SYNC_DATA"
const val SYNC_DATA_WORK_NAME = "SYNC_DATA_WORK_NAME"
const val IS_WORK_NOTIFICATION_ENABLE = "IS_WORK_NOTIFICATION_ENABLE"

class SyncWorker @AssistedInject constructor(
    @Assisted private val appContext: Context,
    @Assisted private val params: WorkerParameters,
    private val useCaseFetch: FetchMoviesUseCase
) : CoroutineWorker(appContext, params) {

    override suspend fun doWork(): Result {
        Log.i(
            SyncWorker::class.simpleName,
            "SyncWorker doWork started"
        )

        useCaseFetch.syncData(getDateTwoWeeksBefore(), getCurrentDate())

        if (inputData.getBoolean(IS_WORK_NOTIFICATION_ENABLE, false)) {
            makeStatusNotification(
                appContext.getString(R.string.new_data_available),
                applicationContext
            )
        }

        Log.i(
            SyncWorker::class.simpleName,
            "SyncWorker doWork end"
        )

        return Result.success()
    }

    @AssistedInject.Factory
    interface Factory : ChildWorkerFactory
}