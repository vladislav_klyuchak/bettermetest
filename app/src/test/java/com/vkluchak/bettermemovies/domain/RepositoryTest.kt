package com.vkluchak.bettermemovies.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.vkluchak.domain.repository.MoviesRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class RepositoryTest {

    @MockK
    lateinit var moviesRepository: MoviesRepository

    @get:Rule
    public var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testSyncData_Exception() = runBlocking {
        coEvery {
            moviesRepository.getMoviesListAsync(any(), any(), any())
        } throws IllegalStateException("err")
        try {
            val list = moviesRepository.getMoviesListAsync(any(), any(), any())
        } catch (e: Exception) {
            assert(e is IllegalStateException)
        }
    }

    @Test
    fun testGetBookmarkMovies_Exception() = runBlocking {
        coEvery { moviesRepository.getBookmarkMoviesLiveData() } throws IllegalStateException("err")
        try {
            val list = moviesRepository.getBookmarkMoviesLiveData()
        } catch (e: Exception) {
            assert(e is IllegalStateException)
        }
    }

}