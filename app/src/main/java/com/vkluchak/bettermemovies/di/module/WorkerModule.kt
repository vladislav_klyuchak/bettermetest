package com.vkluchak.bettermemovies.di.module

import androidx.work.ListenableWorker
import com.squareup.inject.assisted.dagger2.AssistedModule
import com.vkluchak.bettermemovies.worker.ChildWorkerFactory
import com.vkluchak.bettermemovies.worker.SyncWorker
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module(includes = [AssistedInject_SampleAssistedInjectModule::class])
@AssistedModule
interface SampleAssistedInjectModule

@MapKey
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkerKey(val value: KClass<out ListenableWorker>)

@Module
interface WorkerBindingModule {
    @Binds
    @IntoMap
    @WorkerKey(SyncWorker::class)
    fun bindHelloWorldWorker(factory: SyncWorker.Factory): ChildWorkerFactory
}

