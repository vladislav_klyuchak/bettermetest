package com.vkluchak.data.repository.storge

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vkluchak.data.repository.storge.converter.GenreIdsConverter
import com.vkluchak.data.repository.storge.model.MovieDBEntity

@Database(entities = [MovieDBEntity::class], version = 1)
@TypeConverters(GenreIdsConverter::class)
public abstract class MoviesDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao
}
