package com.vkluchak.bettermemovies.ui.favorites

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.vkluchak.bettermemovies.worker.TAG_SYNC_DATA
import com.vkluchak.bettermemovies.worker.createSyncWork
import com.vkluchak.data.repository.storge.model.NetworkState
import com.vkluchak.domain.model.MovieData
import com.vkluchak.domain.repository.MoviesRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoritesViewModel @Inject constructor(
    private val repository: MoviesRepository
) : ViewModel() {

    val networkState = MutableLiveData<NetworkState>()

    var mSavedWorkInfo: LiveData<List<WorkInfo>> =
        WorkManager.getInstance().getWorkInfosByTagLiveData(TAG_SYNC_DATA)

    fun getAllBookmarkMovies() = repository.getBookmarkMoviesLiveData()

    fun setIsBookmark(movieData: MovieData) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.changeBookmarkStatus(movieData.id, !movieData.is_bookmark)
        }
    }

    fun syncData() {
        createSyncWork()
    }

    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        Log.e(FavoritesViewModel::class.java.simpleName, "An error happened: ${e.toString()}")
        networkState.postValue(NetworkState.Failed(e.localizedMessage))
    }
}