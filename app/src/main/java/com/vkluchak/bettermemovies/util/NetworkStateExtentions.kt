package com.vkluchak.bettermemovies.util

import android.app.Activity
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.work.WorkInfo
import com.google.android.material.snackbar.Snackbar
import com.vkluchak.bettermemovies.R
import com.vkluchak.data.repository.storge.model.NetworkState
import kotlinx.android.synthetic.main.activity_main.*

inline fun Activity.networkState(newNetworkState: NetworkState, crossinline f: () -> Unit) {
    nav_host_fragment.visibility = View.VISIBLE
    when (newNetworkState) {
        is NetworkState.Success -> {
            this.progress(false)
        }
        is NetworkState.Failed -> {
            this.progress(false)

            this.snack(newNetworkState) {
                action(" Retry", R.color.colorAccent) {
                    f()
                }
            }
        }
        is NetworkState.Running -> {
            this.progress(true)
        }
    }
}

inline fun Activity.networkState(state: WorkInfo.State, crossinline f: () -> Unit) {
    when (state) {
        WorkInfo.State.FAILED -> {
            this.progress(false)
            this.snack { f() }
        }
        WorkInfo.State.RUNNING -> this.progress(true)
        else -> this.progress(false)
    }
}

inline fun Activity.snack(
    networkState: NetworkState.Failed,
    length: Int = Snackbar.LENGTH_LONG,
    f: Snackbar.() -> Unit
) {
    val message =
        if (networkState.errorMassage.isNullOrBlank()) "Error" else networkState.errorMassage
    val snack = Snackbar.make(
        findViewById<CoordinatorLayout>(R.id.placeSnackBar),
        message.toString(),
        Snackbar.LENGTH_LONG
    )
    snack.f()
    snack.show()
}

inline fun Activity.snack(f: Snackbar.() -> Unit) {
    val message = "Error"
    val snack = Snackbar.make(
        findViewById<CoordinatorLayout>(R.id.placeSnackBar),
        message.toString(),
        Snackbar.LENGTH_LONG
    )
    snack.f()
    snack.show()
}

fun Snackbar.action(action: String, color: Int? = null, listener: (View) -> Unit) {
    setAction(action, listener)
    color?.let { setActionTextColor(ContextCompat.getColor(context, color)) }
}