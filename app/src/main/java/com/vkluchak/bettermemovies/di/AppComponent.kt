package com.vkluchak.bettermemovies.di

import android.app.Application
import com.vkluchak.bettermemovies.BetterMeMoviesApp
import com.vkluchak.bettermemovies.di.module.*
import com.vkluchak.bettermemovies.worker.SampleWorkerFactory
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ContextModule::class,
   //   InterceptorModule::class,
        SampleAssistedInjectModule::class,
        AppModule::class,
        RepositoryModule::class,
        UseCaseModule::class,
        MainActivityModule::class,
        WorkerBindingModule::class]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun factory(): SampleWorkerFactory

    fun inject(betterMeMoviesApp: BetterMeMoviesApp)
}
