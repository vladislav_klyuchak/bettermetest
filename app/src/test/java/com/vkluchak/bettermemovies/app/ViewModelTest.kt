package com.vkluchak.bettermemovies.app

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.github.testcoroutinesrule.TestCoroutineRule
import com.nhaarman.mockitokotlin2.*
import com.vkluchak.bettermemovies.ui.dashboard.DashboardViewModel
import com.vkluchak.data.repository.storge.model.NetworkState
import com.vkluchak.domain.repository.MoviesRepository
import com.vkluchak.domain.usecase.FetchMoviesUseCase
import kotlinx.coroutines.delay
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var dashboardViewModel: DashboardViewModel

    @Mock
    private lateinit var useCaseFetch: FetchMoviesUseCase

    @Mock
    private lateinit var repository: MoviesRepository

    @Mock
    private var viewStateObserver: Observer<NetworkState> = mock()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        dashboardViewModel = DashboardViewModel(
            repository

        ).apply {
            this.networkState.observeForever(viewStateObserver)
        }
    }

    @Test
    fun `should change NetworkStatus when syncData Server trows exception`() =
        testCoroutineRule.runBlockingTest {
            // Given
            val error = Error()
            whenever(useCaseFetch.syncData(any(), any())).thenThrow(error)

            // When
            dashboardViewModel.syncData()
            delay(10)
            // Then
            verify(viewStateObserver).onChanged(NetworkState.Running)
            verify(viewStateObserver, timeout(500)).onChanged(NetworkState.Failed(null))
        }

    @Test
    fun `should change NetworkStatus when syncData Success`() =
        testCoroutineRule.runBlockingTest {
            // Given
            whenever(useCaseFetch.syncData(any(), any())).thenReturn(Unit)

            // When
            dashboardViewModel.syncData()
            delay(10)
            // Then
            verify(viewStateObserver).onChanged(NetworkState.Running)
            verify(viewStateObserver, timeout(500)).onChanged(NetworkState.Success)
        }

}