package com.vkluchak.bettermemovies.di.module

import com.vkluchak.bettermemovies.ui.dashboard.DashboardFragment
import com.vkluchak.bettermemovies.ui.favorites.FavoritesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeFavoritesFragment(): FavoritesFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardFragment(): DashboardFragment

}
