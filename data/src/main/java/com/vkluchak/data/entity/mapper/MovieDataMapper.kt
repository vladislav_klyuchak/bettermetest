package com.vkluchak.data.entity.mapper

import com.vkluchak.data.entity.MovieEntity
import com.vkluchak.data.repository.storge.model.MovieDBEntity
import com.vkluchak.domain.model.Mapper
import com.vkluchak.domain.model.MovieData

class MovieDataMapper : Mapper<MovieEntity, MovieData> {
    override fun map(source: MovieEntity): MovieData {
        return MovieData(
            source.adult,
            source.backdropPath,
            source.genreIds,
            source.id,
            source.originalLanguage,
            source.originalTitle,
            source.overview,
            source.popularity.toFloat(),
            source.posterPath,
            source.releaseDate,
            source.title,
            source.video,
            source.voteAverage.toFloat(),
            source.voteCount
        )
    }
}

class MovieDataBaseMapper : Mapper<MovieData, MovieDBEntity> {
    override fun map(source: MovieData): MovieDBEntity {
        return MovieDBEntity(
            source.id,
            source.vote_count,
            source.video,
            source.vote_average,
            source.title,
            source.popularity,
            source.poster_path,
            source.original_language,
            source.original_title,
            source.backdrop_path,
            source.adult,
            source.overview,
            source.release_date,
            source.genre_ids
        )
    }
}

class DbMovieDataMapper : Mapper<MovieDBEntity, MovieData> {
    override fun map(source: MovieDBEntity): MovieData {
        return MovieData(
            source.adult,
            source.backdropPath,
            source.genreIds,
            source.id,
            source.originalLanguage,
            source.originalTitle,
            source.overview,
            source.popularity.toFloat(),
            source.posterPath,
            source.releaseDate,
            source.title,
            source.video,
            source.voteAverage.toFloat(),
            source.voteCount
        )
    }
}