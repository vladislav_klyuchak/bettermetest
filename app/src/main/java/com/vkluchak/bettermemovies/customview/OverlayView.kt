package com.vkluchak.bettermemovies.customview

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.FrameLayout

class OverlayView(context: Context?, attrs: AttributeSet?) : FrameLayout(context!!, attrs) {

    init {
        setBackgroundColor(Color.WHITE)
        alpha = 0.4f

        setOnTouchListener { v, event -> true }

    }
}