package com.vkluchak.bettermemovies.di.module

import com.vkluchak.domain.repository.MoviesRepository
import com.vkluchak.domain.usecase.FetchMoviesUseCase
import com.vkluchak.domain.usecase.FetchMoviesUseCaseImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Provides
    @Singleton
    internal fun provideMoviesUseCase(
        moviesRepository: MoviesRepository
    ) : FetchMoviesUseCase {
        return FetchMoviesUseCaseImpl(moviesRepository)
    }

}